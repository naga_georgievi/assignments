Инсталиран софтуер за нуждите на задачата:

*   [heroku-cli](https://devcenter.heroku.com/articles/heroku-cli)

# Задача

Създаване на бот регистриран в [Azure Bot Service](https://dev.botframework.com/), който отговаря на въпроси, чрез достъп до световни факти и данни посредством [Wolfram Alpha API](https://products.wolframalpha.com/api/). Бота трябва да е качен на облачната PaaS платформа [Heroku](https://www.heroku.com/).

## Инструкции

*   Направете регистрация на адрес [https://www.heroku.com](https://www.heroku.com/)

*   Клонирайте изходния код на бота от bitbucket
	````bash
	git clone https://bitbucket.org/naga_georgievi/nagaskypebot.git
	````
*   сменете работната директория
	````bash
	cd nagaskypebot
	````
*   Установете връзка с heroku като използвате потребителското име и парола от регистрацията
	````bash
	heroku login
	````
*   Създайте ново приложение в heroku
	````bash
	heroku create
	````
*   Добавете следния пакет (buildpack) към създаденото приложение
    ````bash
    heroku buildpacks:add heroku/nodejs
    ````
*   Задайте променлива на средата на heroku да инсталира само зависимостите 
    ````bash
    heroku config:set NPM_CONFIG_PRODUCTION=true
    ````
*   копирайте файла .env.schema в .env
    ````bash
    cp .env.schema .env
    ````
*   Направете регистрация за Microsoft Azure на адрес [https://azure.microsoft.com/en-us/free/students/](https://azure.microsoft.com/en-us/free/students/)

*   Добавете Azure Bot ресурс и конфигурирайте Messaging endpoint (<адрес на приложението в heroku>/api/messages). **Внимание** изберете "F0 Free" план за плащане

*   Добавете Skype канал и го конфигурирайте - Messaging (enable messaging)

*   Копирайте App ID и парола от Azure Bot регистрацията

*   Направете регистрация на адрес [https://products.wolframalpha.com/api/](https://products.wolframalpha.com/api/) и вземете API key за достъп до WolframAlpha API

*   редактирайте файла .env като добавите API ключовете

*   добави новите дайлове към локалното git хранилище
    ````bash
    git add -A
    ````
*   Запази направените промени в локалното git хранилище
    ````bash
    git commit -m "added api keys"
    ````
*   Качете кода в отдалеченото git хранилище на проекта в heroku в branch master
    ````bash
    git push heroku master
    ````
*   Добави бота към скайп контакти и тествай